package consumer

import (
	"fmt"
	"github.com/IBM/sarama"
	"log"
)

func main() {
	// общий конфиг для консьюмера и продюсера
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Consumer.Return.Errors = true

	// инициализируем консьюмер
	consumer, err := sarama.NewConsumer([]string{"localhost:9092"}, config)
	if err != nil {
		log.Fatalln(err)
	}
	defer consumer.Close()

	// подписываемся на раздел 0 топика "topic", начиная с последнего сообщения
	partitionConsumer, err := consumer.ConsumePartition("topic", 0, sarama.OffsetNewest)
	if err != nil {
		log.Fatalln(err)
	}
	defer partitionConsumer.Close()

	// инициализируем продюсер
	producer, err := sarama.NewSyncProducer([]string{"localhost:9092"}, config)
	if err != nil {
		log.Fatalln(err)
	}
	//defer producer.Close()

	for {
		select {
		// ждем сообщения
		case msg := <-partitionConsumer.Messages():
			log.Printf("Message is stored in topic %s partition %d at offset %d", msg.Topic, msg.Partition, msg.Offset)
			log.Printf("Message is %s", msg.Value)

			// создаем и отправляем ответ
			response := &sarama.ProducerMessage{
				Topic: "topic",
				Value: sarama.StringEncoder(fmt.Sprintf("answer fo mesage %s", msg.Value)),
			}
			partition, offset, err := producer.SendMessage(response)
			if err != nil {
				log.Println("answer error", err)
			} else {
				log.Println("answer success", partition, offset)
			}
		case err := <-partitionConsumer.Errors():
			log.Fatalln("error", err)
		}
	}
}
