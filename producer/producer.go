package main

import (
	"github.com/IBM/sarama"
	"log"
)

func main() {
	// общий конфиг для продюсера
	config := sarama.NewConfig()
	// включаем возврат успешных сообщений
	config.Producer.Return.Successes = true

	// инициализируем продюсер
	producer, err := sarama.NewSyncProducer([]string{"localhost:9092"}, config)
	if err != nil {
		log.Fatalln(err)
	}
	defer producer.Close()

	// создаем сообщение для отправки в тему "topic"
	message := &sarama.ProducerMessage{
		Topic: "topic",
		Value: sarama.StringEncoder("Hello"),
	}

	// отправляем сообщение

	// partition - часть темы (topic) в Kafka. Тема может быть разделена на несколько разделов, каждый из которых
	// содержит упорядоченный и неизменяемый поток сообщений. Разделы позволяют параллельно обрабатывать и хранить сообщения в Kafka,
	// а также обеспечивают масштабируемость и отказоустойчивость системы.

	// offset (смещение) - это уникальный идентификатор для каждого сообщения в разделе.
	// Он указывает позицию сообщения в разделе и используется для определения порядка сообщений и контроля прогресса потребления.
	partition, offset, err := producer.SendMessage(message)
	if err != nil {
		log.Fatalln(err)
	}

	// выводим информацию о сообщении
	log.Printf("Message is stored in topic %s partition %d at offset %d", message.Topic, partition, offset)
}
